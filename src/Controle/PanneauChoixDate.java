package Controle;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import Question.Question;

public class PanneauChoixDate extends JPanel implements PanneauChoix{

	private JButton validation;
	private JComboBox jourDeb, moisDeb, anneeDeb, jourFin, moisFin, anneeFin;
	private Question aff;
	
	private String[] tabJour = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	private String[] tabMois = {"1","2","3","4","5","6","7","8","9","10","11","12"};
	private String[] tabAnnee = {"2010","2011","2012","2013","2014","2015","2016","2017"};
	
	public PanneauChoixDate(Question ap,String s){
		
		/**********************************
		* Cr�ation des bords des composants
		***********************************/
		Border raisedbevel 	= BorderFactory.createRaisedBevelBorder();
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		Border compound		= BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);
		this.setBorder(compound);
		
		aff=ap;
		
		JLabel l=new JLabel(s, JLabel.CENTER);
		
		jourDeb=new JComboBox(tabJour);
		moisDeb=new JComboBox(tabMois);
		anneeDeb=new JComboBox(tabAnnee);
		
		jourFin=new JComboBox(tabJour);
		moisFin=new JComboBox(tabMois);
		anneeFin=new JComboBox(tabAnnee);
		
		JPanel dateDeb=new JPanel();
		JLabel debut=new JLabel("Du : ");
		dateDeb.add(debut);
		dateDeb.add(jourDeb);
		dateDeb.add(moisDeb);
		dateDeb.add(anneeDeb);
		
		JPanel dateFin=new JPanel();
		JLabel fin=new JLabel("au : ");
		dateFin.add(fin);
		dateFin.add(jourFin);
		dateFin.add(moisFin);
		dateFin.add(anneeFin);
		
		validation=new JButton("Valider");
		validation.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				try {
					aff.effectuerChangement(jourDeb.getSelectedItem()+"/"+moisDeb.getSelectedItem()+"/"+anneeDeb.getSelectedItem()+" "
							+jourFin.getSelectedItem()+"/"+moisFin.getSelectedItem()+"/"+anneeFin.getSelectedItem());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.out.println("Erreur dans le format de la date");
				}
				
				Window window = SwingUtilities.windowForComponent(PanneauChoixDate.this);
				if (window instanceof JFrame) {
					JFrame frame = (JFrame) window;
			 
					//frame.setVisible(false);
					//frame.dispose();
				}
			}
		});
		
		
		
		this.setLayout(new GridLayout(4,1));
		this.add(l);
		this.add(dateDeb);
		this.add(dateFin);
		this.add(validation);
	}
	
	public void init(){
		
	}
	
}
