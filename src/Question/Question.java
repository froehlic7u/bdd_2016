package Question;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;
	
public abstract class Question extends Observable{
		public abstract ResultSet getResult();
		public abstract void effectuerChangement(String s) throws SQLException;

}
