package Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class Question3 extends Question{
	
	private Connection con;
	private ResultSet res;
	
	public Question3(Connection c){
		con=c;
	}
	
	public void effectuerChangement(String s) throws SQLException{
		PreparedStatement stm=con.prepareStatement("select nomserv,dataff from SERVEUR natural join affecter where dataff between ? and ? and numtab=?");
		stm.setString(1, s.split(" ")[0]);
		stm.setString(2, s.split(" ")[1]);
		stm.setString(3, s.split(" ")[2]);
		res = stm.executeQuery();
		
		setChanged();
		notifyObservers();
		
		stm.close();
	}
	
	public ResultSet getResult(){
		return res;
	}

}
