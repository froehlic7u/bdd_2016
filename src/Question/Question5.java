package Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class Question5 extends Question{

	private Connection con;
	private ResultSet res;
	
	public Question5(Connection c){
		con=c;
	}
	
	public void effectuerChangement(String s) throws SQLException{
		PreparedStatement stm=con.prepareStatement("SELECT nomserv FROM serveur WHERE nomserv NOT IN (SELECT nomserv FROM serveur NATURAL JOIN affecter NATURAL JOIN commande WHERE datcom BETWEEN ? AND ?) ORDER BY nomserv");
		stm.setString(1, s.split(" ")[0]);
		stm.setString(2, s.split(" ")[1]);
		res = stm.executeQuery();
		
		setChanged();
		notifyObservers();
		
		stm.close();
	}
	
	public ResultSet getResult(){
		return res;
	}
	
}
