package Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class Question2 extends Question{

	private Connection con;
	private ResultSet res;
	
	public Question2(Connection c){
		con=c;
	}
	
	public void effectuerChangement(String s) throws SQLException{
		PreparedStatement stm=con.prepareStatement("SELECT numplat, libelle FROM plat WHERE numplat NOT IN (SELECT numplat FROM commande natural join contient WHERE datcom BETWEEN ? AND ? )");
		stm.setString(1, s.split(" ")[0]);
		stm.setString(2, s.split(" ")[1]);
		res = stm.executeQuery();
		
		setChanged();
		notifyObservers();
		
		stm.close();
	}
	
	public ResultSet getResult(){
		return res;
	}
	
}
