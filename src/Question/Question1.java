package Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class Question1 extends Question{

	private Connection con;
	private ResultSet res;
	
	public Question1(Connection c){
		con=c;
	}
	
	public void effectuerChangement(String s) throws SQLException{
		PreparedStatement stm=con.prepareStatement("select distinct numplat,libelle "
				+ "from plat natural join contient natural join commande "
				+ "where datcom between ? and ? order by numplat asc");
		stm.setString(1, s.split(" ")[0]);
		stm.setString(2, s.split(" ")[1]);
		res = stm.executeQuery();
		
		setChanged();
		notifyObservers();
		
		stm.close();
	}
	
	public ResultSet getResult(){
		return res;
	}
	
}
