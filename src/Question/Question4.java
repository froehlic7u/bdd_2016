package Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class Question4 extends Question{

	private Connection con;
	private ResultSet res;
	
	public Question4(Connection c){
		con=c;
	}
	
	public void effectuerChangement(String s) throws SQLException{
		PreparedStatement stm=con.prepareStatement("select nomserv,sum(prixunit*quantite) as CA,count(numCom) as NbCommandes from commande natural join contient natural join affecter natural join plat natural join serveur where datcom between ? and ? group by nomserv,numserv order by sum(prixunit*quantite) desc");
		stm.setString(1, s.split(" ")[0]);
		stm.setString(2, s.split(" ")[1]);
		res = stm.executeQuery();
		
		setChanged();
		notifyObservers();
		
		stm.close();
	}
	
	public ResultSet getResult(){
		return res;
	}

}
