package Principale;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Controle.PanneauChoixDate;
import Controle.PanneauChoixTableDate;
import Question.Question1;
import Question.Question2;
import Question.Question3;
import Question.Question4;
import Question.Question5;
import affichage.Affichage;

public class Main {
	public static void main(String[] args) {
		JFrame jf= new JFrame();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Acces acces=new Acces("root","root");
		Question1 ap1=new Question1(acces.getConnection());
		Question2 ap2=new Question2(acces.getConnection());
		Question3 ap3=new Question3(acces.getConnection());
		Question4 ap4=new Question4(acces.getConnection());
		Question5 ap5=new Question5(acces.getConnection());
		
		Affichage aff=new Affichage();
		
		ap1.addObserver(aff);
		ap2.addObserver(aff);
		ap3.addObserver(aff);
		ap4.addObserver(aff);
		ap5.addObserver(aff);
		
		JPanel principale=new JPanel();
		principale.setLayout(new BorderLayout());
			
		JPanel q1=new JPanel();
		q1.setLayout(new GridLayout(3,1));
		JPanel q2=new JPanel();
		q2.setLayout(new GridLayout(3,1));
				
		JButton b3=new JButton("Question 3");
		
		JButton b5=new JButton("Question 5");
		
		JButton b6=new JButton("Question 6");
		
		
		q1.setPreferredSize(new Dimension(500, 480));
		q1.add(new PanneauChoixDate(ap1,"Affichage des plats servis"));
		q1.add(new PanneauChoixDate(ap2,"Affichage qui n'ont jamais �t� servis"));
		q1.add(new PanneauChoixTableDate(ap3,"Affichage des affectations des serveurs",acces.getConnection()));
		
		q2.setPreferredSize(new Dimension(500, 480));
		q2.add(new PanneauChoixDate(ap4,"Affichage du chiffre d'affaire et le nombre des commandes des serveurs"));
		q2.add(new PanneauChoixDate(ap5,"Affichage des serveurs n'ayant pas r�alis� de chiffre d'affaire"));
		q2.add(b6);
		
		principale.add(q1,BorderLayout.WEST);
		principale.add(aff,BorderLayout.CENTER);
		principale.add(q2,BorderLayout.EAST);
		
		
		jf.setContentPane(principale);
		jf.pack();
		jf.setResizable(false);
		jf.setVisible(true);
	}
}
