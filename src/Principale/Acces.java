package Principale;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class Acces {

	private Connection c;
	private String user, password;

	public Acces(String u, String p){
		user=u;	
		password=p;
	}

	public Connection getConnection(){
		if(c==null){
			try{
				c=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE",user,password);
			}catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Connexion � la base de donn�e impossible ..! ", JOptionPane.ERROR_MESSAGE);
			}
		}
		return c;
	}
}
