package affichage;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import Question.Question;

public class Affichage extends JPanel implements Observer{

	private boolean aff=false;
	private String res;

	public Affichage() {
		
		/**********************************
		* Cr�ation des bords des composants
		***********************************/
		Border raisedbevel 	= BorderFactory.createRaisedBevelBorder();
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		Border compound		= BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);
		
		// TODO Auto-generated constructor stub
		this.setPreferredSize(new Dimension(480, 480));
		this.setBackground(Color.WHITE);
		this.setBorder(compound);
		this.setLayout(new BorderLayout());
	}

	@Override
	public void update(Observable o, Object arg) {
		
		ResultSet res=((Question)o).getResult();
		ArrayList<ArrayList<Object>> d=new ArrayList<ArrayList<Object>>();
		int nbCol=0;
		
		try {
			ResultSetMetaData rm=res.getMetaData();
			nbCol=rm.getColumnCount();
			
			for(int i=0 ; i<nbCol ; i++){
				d.add(new ArrayList<Object>());
			}
			
			while(res.next()){
				String resultat="";
				for(int i=0; i<nbCol ; i++){
					d.get(i).add(res.getString(i+1));
				}
			}
			
			Object[][] test=new Object[d.get(0).size()][d.size()];
			for(int i=0 ; i<d.get(0).size() ; i++){
				for(int j=0 ; j<d.size() ; j++){
					test[i][j]=d.get(j).get(i);
				}
			}
			
			this.removeAll();
			Object[] nom=new Object[nbCol];
			for(int i=0 ; i<nbCol ; i++){
				nom[i]=rm.getColumnName(i+1);
			}
			this.add(new JScrollPane(new JTable(test, nom)),BorderLayout.CENTER);
			revalidate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*ArrayList<ArrayList<Object>> al=new ArrayList<ArrayList<Object>>();
		for(int i=0 ; i<3 ; i++){
			al.add(new ArrayList<Object>());
		}
		al.get(0).add("1");
		al.get(0).add("2");
		al.get(1).add("3");
		al.get(1).add("4");
		al.get(2).add("5");
		al.get(2).add("6");
		
		Object[][] test=new Object[2][3];
		for(int i=0 ; i<2 ; i++){
			for(int j=0 ; j<3 ; j++){
				test[i][j]=al.get(j).get(i);
			}
		}
		
		for(int i=0 ; i<2 ; i++){
			for(int j=0 ; j<3 ; j++){
				System.out.print(test[i][j]);
			}
			System.out.println("\n");
		}
		
		Object[] nom={"test1","test2","test3"};
		this.add(new JScrollPane(new JTable(test, nom)),BorderLayout.CENTER);
		revalidate();*/

	}
	/*
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub

		ResultSet res=((QuestionPlat)o).getResult();
		int nbCol=0;
		try {
			ResultSetMetaData rm=res.getMetaData();
			nbCol=rm.getColumnCount();
			String entete="";
			for(int i=1; i<nbCol ; i++){
				entete+=rm.getColumnName(i)+" | ";
			}
			entete+=rm.getColumnName(nbCol);
			System.out.println(entete);
			for(int i=0 ; i<entete.length() ; i++)
				System.out.print("-");
			System.out.println();
			while(res.next()){
				String resultat="";
				for(int i=1; i<=nbCol ; i++){
					resultat+=res.getString(i)+" ";
				}

				System.out.println(resultat);
			}
			res.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		aff=true;
		repaint();

	}
	*/
}
